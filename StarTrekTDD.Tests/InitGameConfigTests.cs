﻿using FluentAssertions;
using NUnit.Framework;

namespace StarTrekTDD.Tests
{
    public class InitGameConfigTests
    {
        [Test]
        public void KlingonShipsNumberInGalaxy_Should_BeEqualTo_7()
        {
            InitGameConfig.KlingonShipsNumberInGalaxy.Should().Be(7);
        }

        [Test]
        public void StarFleetBasesNumberInGalaxy_Should_BeEqualTo_2()
        {
            InitGameConfig.StarFleetBasesNumberInGalaxy.Should().Be(2);
        }

        [Test]
        public void StarsNumberInGalaxy_Should_BeEqualTo_20()
        {
            InitGameConfig.StarsNumberInGalaxy.Should().Be(20);
        }

        [Test]
        public void EnterprisesNumberInGalaxy_Should_BeEqualTo_1()
        {
            InitGameConfig.EnterprisesNumberInGalaxy.Should().Be(1);
        }

        [Test]
        public void StellarDates_Should_BeEqualTo_30()
        {
            InitGameConfig.StellarDates.Should().Be(30);
        }

        [Test]
        public void EnergyUnits_Should_BeEqualTo_600()
        {
            InitGameConfig.EnergyUnits.Should().Be(600);
        }

        [Test]
        public void GridSideSize_Should_BeEqualTo_8()
        {
            InitGameConfig.GridSideSize.Should().Be(8);
        }

        [Test]
        public void QuadrantsNumberInGalaxy_Should_BeEqualTo_SideOfGridSideToPowerOfTwo()
        {
            InitGameConfig.QuadrantsNumberInGalaxy.Should()
                .Be(InitGameConfig.GridSideSize * InitGameConfig.GridSideSize);
        }

        [Test]
        public void SectorsNumberInQuadrant_Should_BeEqualTo_SideOfGridSideToPowerOfTwo()
        {
            InitGameConfig.SectorsNumberInQuadrant.Should()
                .Be(InitGameConfig.GridSideSize * InitGameConfig.GridSideSize);
        }

        [Test]
        public void KlingonShipEnergyUnits_Should_BeEqualTo_100()
        {
            InitGameConfig.KlingonShipEnergyUnits.Should().Be(100);
        }
    }
}