﻿using FluentAssertions;
using NUnit.Framework;
using StarTrekTDD.Map;
using StarTrekTDD.Map.Enums;
using StarTrekTDD.Map.Structures;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace StarTrekTDD.Tests.Map
{
    public class GalaxyTests : TestBase
    {
        private Galaxy _galaxy;

        [SetUp]
        public void Setup()
        {
            _galaxy = new Galaxy();
        }

        [Test]
        public void EnterprisesNumber_Should_BeSameAs_InGameInitConfig()
        {
            _galaxy.Sectors
                .Count(s => s.SpaceObject.Type == SpaceObjectType.Enterprise)
                .Should()
                .Be(InitGameConfig.EnterprisesNumberInGalaxy);
        }

        [Test]
        public void KlingonShipsNumber_Should_BeSameAs_InGameInitConfig()
        {
            _galaxy.Sectors
                .Count(s => s.SpaceObject.Type == SpaceObjectType.KlingonShip)
                .Should()
                .Be(InitGameConfig.KlingonShipsNumberInGalaxy);
        }

        [Test]
        public void StarsNumber_Should_BeSameAs_InGameInitConfig()
        {
            _galaxy.Sectors
                .Count(s => s.SpaceObject.Type == SpaceObjectType.Star)
                .Should()
                .Be(InitGameConfig.StarsNumberInGalaxy);
        }

        [Test]
        public void StarFleetBasesNumber_Should_BeSameAs_InGameInitConfig()
        {
            _galaxy.Sectors
                .Count(s => s.SpaceObject.Type == SpaceObjectType.StarFleetBase)
                .Should()
                .Be(InitGameConfig.StarFleetBasesNumberInGalaxy);
        }

        [Test]
        public void SectorsNumber_Should_BeEqualTo_MultiplyQuadrantsNumberInGalaxyAndSectorsNumberInQuadrant()
        {
            _galaxy.Sectors.Count()
                .Should()
                .Be(InitGameConfig.QuadrantsNumberInGalaxy * InitGameConfig.SectorsNumberInQuadrant);
        }

        [Test]
        public void QuadrantsNumber_Should_BeSameAs_InGameInitConfig()
        {
            _galaxy.Quadrants.Count().Should().Be(InitGameConfig.QuadrantsNumberInGalaxy);
        }

        [Test]
        public void EnterpriseStartPosition_Should_BeIn_GalaxyCenter()
        {
            _galaxy.EnterpriseSector.Vector
                .Should()
                .Be(new Vector2((int)_galaxy.Sectors.Max(s => s.Vector.X) / 2, (int)_galaxy.Sectors.Max(s => s.Vector.Y) / 2));
        }

        [Test]
        public void ShuffledQuadrant_Should_NotBeSameAs_BeforeShuffle()
        {
            var quadrant = _galaxy.Quadrants.First();
            var currentQuadrantSectorSpaceObjects = _galaxy.Sectors
                .Where(s => quadrant.SectorVectors.Contains(s.Vector))
                .Select(s => s.SpaceObject)
                .ToList();

            _galaxy.ShuffleQuadrant(quadrant.Vector);

            var shuffledQuadrantSectorSpaceObjects = _galaxy.Sectors
                .Where(s => quadrant.SectorVectors.Contains(s.Vector))
                .Select(s => s.SpaceObject)
                .ToList();
            shuffledQuadrantSectorSpaceObjects.Should().NotBeSameAs(currentQuadrantSectorSpaceObjects);
        }

        [Test]
        public void CalculatedDistance_Should_Be_Correctly()
        {
            var firstSector = _galaxy.Sectors.First();
            var moveDistance = new Vector2(
                x: InitGameConfig.GridSideSize + 1,
                y: 1);

            var result = _galaxy.CalculateDistance(firstSector.Vector, moveDistance);

            result.StartPosition.Should().Be(new Position(firstSector.Vector, new Vector2(0, 0)));
            result.EndPosition.Should().Be(new Position(firstSector.Vector + moveDistance, new Vector2(1, 0)));
            result.MovedSectors.Should().Be(InitGameConfig.GridSideSize + 2);
            result.MovedQuadrants.Should().Be(1);
        }

        [Test]
        public void UpdatedSectorSpaceObject_Should_HasSetValue()
        {
            var sector = _galaxy.Sectors.First();
            var expectedNewSectorSpaceObject = Fixture.CreateExcept(sector.SpaceObject);

            _galaxy.UpdateSectorSpaceObject(sector, expectedNewSectorSpaceObject);

            _galaxy.Sectors.First().SpaceObject.Should().Be(expectedNewSectorSpaceObject);
        }

        [Test]
        public void If_GalaxyHasNoKlingonShip_Than_Result_Should_BeFalse()
        {
            RemoveKlingonShips(_galaxy);

            var result = _galaxy.IsAnyKlingonShipOutsideEnterpriseQuadrant();

            result.Should().BeFalse();
        }

        [TestCase(-1, 0, true)]
        [TestCase(1, 0, true)]
        [TestCase(0, 1, true)]
        [TestCase(0, -1, true)]
        [TestCase(1, -1, false)]
        [TestCase(1, 1, false)]
        [TestCase(-1, 1, false)]
        [TestCase(-1, -1, false)]
        public void If_EnterpriseIsNextToStarFleetBase_Than_Result_Should_BeTrue(int x, int y, bool expectedResult)
        {
            var starFleetSector = _galaxy.StarFleetBaseSectors.First();
            var move = new Vector2(x, y);
            var targetPosition = starFleetSector.Vector + move;
            var targetPositionExists = _galaxy.Sectors.Any(s => s.Vector == targetPosition);

            if (!targetPositionExists)
                return;

            _galaxy.MoveEnterprise(starFleetSector.Vector + move);

            var result = _galaxy.IsEnterpriseNextToStarFleetBase();

            result.Should().Be(expectedResult);
        }

        [Test]
        public void EnterprisePositionAfterMove_Should_BeAsGiven()
        {
            var targetPosition = new Vector2(1, 1);

            _galaxy.MoveEnterprise(targetPosition);

            _galaxy.EnterpriseSector.Vector.Should().Be(targetPosition);
        }

        [Test]
        public void EnterprisePosition_Should_BeCorrect()
        {
            var enterprisePosition = _galaxy.Sectors.Where(s => s.SpaceObject.Type == SpaceObjectType.Enterprise);

            _galaxy.EnterpriseSector.Should().Be(enterprisePosition.First());
        }

        [Test]
        public void KlingonShipPositions_Should_BeCorrect()
        {
            var klingonShipPositions = _galaxy.Sectors.Where(s => s.SpaceObject.Type == SpaceObjectType.KlingonShip);

            _galaxy.KlingonShipSectors.Should().BeEquivalentTo(klingonShipPositions);
        }

        [Test]
        public void StarPositions_Should_BeCorrect()
        {
            var starPositionsPositions = _galaxy.Sectors.Where(s => s.SpaceObject.Type == SpaceObjectType.Star);

            _galaxy.StarSectors.Should().BeEquivalentTo(starPositionsPositions);
        }

        [Test]
        public void StarFleetBasePositions_Should_BeCorrect()
        {
            var starFleetBasePositions = _galaxy.Sectors.Where(s => s.SpaceObject.Type == SpaceObjectType.StarFleetBase);

            _galaxy.StarFleetBaseSectors.Should().BeEquivalentTo(starFleetBasePositions);
        }

        [Test]
        public void SectorWithSpaceObjectDifferentThanEmptiness_Should_NotBeEmpty()
        {
            var notEmptySector = _galaxy.Sectors.First(s => s.SpaceObject.Type != SpaceObjectType.Emptiness);

            var result = _galaxy.IsSectorEmpty(notEmptySector.Vector);

            result.Should().BeFalse();
        }

        [Test]
        public void SectorWithEmptinessSpaceObjectDifferent_Should_BeEmpty()
        {
            var emptySector = _galaxy.Sectors.First(s => s.SpaceObject.Type == SpaceObjectType.Emptiness);

            var result = _galaxy.IsSectorEmpty(emptySector.Vector);

            result.Should().BeTrue();
        }

        [Test]
        public void IsAnyKlingonShipInsideEnterpriseQuadrant_Should_ReturnFalse_If_ThereIsNoKlingonShipInsideEnterpriseQuadrant()
        {
            RemoveKlingonShips(_galaxy);

            var result = _galaxy.IsAnyKlingonShipInsideEnterpriseQuadrant();

            result.Should().BeFalse();
        }

        [Test]
        public void IsAnyKlingonShipInsideEnterpriseQuadrant_Should_ReturnTrue_If_ThereIsKlingonShipInsideEnterpriseQuadrant()
        {
            MoveEnterpriseToKlingonShipQuadrant(_galaxy);

            var result = _galaxy.IsAnyKlingonShipInsideEnterpriseQuadrant();

            result.Should().BeTrue();
        }

        [Test]
        public void GetEnterpriseSectorClosetKlingonShipSector_Should_ReturnClosetKlingonShipSector()
        {
            MoveEnterpriseToKlingonShipQuadrant(_galaxy);
            var closetKlingonShip = GetEnterpriseSectorClosetKlingonShipSector(_galaxy);

            var result = _galaxy.GetEnterpriseSectorClosetKlingonShipSector();

            result.Should().BeEquivalentTo(closetKlingonShip);
        }

        [Test]
        public void EnterpriseQuadrantSurroundingsInfo_Should_ReturnInfo_About_QuadrantsAroundEnterpriseQuadrant_And_EnterpriseQuadrant()
        {
            var enterprisePositionQuadrant = GetEnterprisePositionQuadrant(_galaxy);
            var quadrantSurroundings = GetQuadrantSurroundings(_galaxy, enterprisePositionQuadrant);
            var sectors = GetSectors(_galaxy, quadrantSurroundings);

            var result = _galaxy.GetEnterpriseQuadrantSurroundingsInfo();

            result.KlingonShipsNumber.Should().Be(sectors.Count(s => s.SpaceObject.Type == SpaceObjectType.KlingonShip));
            result.StarFleetBasesNumber.Should().Be(sectors.Count(s => s.SpaceObject.Type == SpaceObjectType.StarFleetBase));
            result.StarsNumber.Should().Be(sectors.Count(s => s.SpaceObject.Type == SpaceObjectType.Star));
        }

        [Test]
        public void GetKlingonShipSectorsInEnterpriseQuadrant_Should_Return_KlingonShipSectorsInsideEnterpriseQuadrant()
        {
            var klingonShip = _galaxy.KlingonShipSectors.First();
            MoveEnterpriseToEmptyQuadrantSector(_galaxy, klingonShip);
            var klingonShipsInQuadrant = GetQuadrantSectors(klingonShip)
                .Where(s => s.SpaceObject.Type == SpaceObjectType.KlingonShip);

            var result = _galaxy.GetKlingonShipSectorsInEnterpriseQuadrant();

            result.Should().BeEquivalentTo(klingonShipsInQuadrant);
        }

        [TestCase(0, 0, true)]
        [TestCase(63, 63, true)]
        [TestCase(64, 64, false)]
        [TestCase(-1, -1, false)]
        public void If_SectorExists_Than_Result_Should_BeTrue(int x, int y, bool expectedResult)
        {
            var result = _galaxy.SectorExists(new Vector2(x, y));

            result.Should().Be(expectedResult);
        }

        private static void MoveEnterpriseToKlingonShipQuadrant(Galaxy galaxy)
        {
            var klingonShip = galaxy.KlingonShipSectors.First();
            var klingonShipQuadrant = galaxy.Quadrants.Single(q => q.SectorVectors.Contains(klingonShip.Vector));
            var emptySector = klingonShipQuadrant.SectorVectors.First(qs =>
                galaxy.Sectors.Single(s => s.Vector == qs).SpaceObject.Type == SpaceObjectType.Emptiness);
            galaxy.MoveEnterprise(emptySector);
        }

        private static Sector GetEnterpriseSectorClosetKlingonShipSector(Galaxy galaxy)
        {
            var result = new Sector();
            var closetSectorsDistance = int.MaxValue;

            foreach (var klingonShipPosition in galaxy.KlingonShipSectors)
            {
                var move = klingonShipPosition.Vector - galaxy.EnterpriseSector.Vector;
                var distance = galaxy.CalculateDistance(galaxy.EnterpriseSector.Vector, move);

                if (distance.MovedSectors < closetSectorsDistance)
                {
                    result = klingonShipPosition;
                    closetSectorsDistance = distance.MovedSectors;
                }
            }

            return result;
        }

        private static IEnumerable<Quadrant> GetQuadrantSurroundings(Galaxy galaxy, Quadrant quadrant)
        {
            var result = new List<Quadrant>();
            var surroundings = new List<int> { -1, 0, 1 };
            result.Add(quadrant);

            foreach (var sur1 in surroundings)
            {
                foreach (var sur2 in surroundings)
                {
                    var move = new Vector2(sur1, sur2);
                    var targetQuadrantPosition = quadrant.Vector + move;

                    var quadrantSurrounding = galaxy.Quadrants.SingleOrDefault(q => q.Vector == targetQuadrantPosition);

                    if (quadrantSurrounding.SectorVectors.Any())
                        result.Add(quadrantSurrounding);
                }
            }

            return result;
        }

        private static Quadrant GetEnterprisePositionQuadrant(Galaxy galaxy) =>
            galaxy.Quadrants.Single(q => q.SectorVectors.Any(s => s == galaxy.EnterpriseSector.Vector));

        private static IEnumerable<Sector> GetSectors(Galaxy galaxy, IEnumerable<Quadrant> quadrants)
        {
            var quadrantVectors = quadrants.SelectMany(q => q.SectorVectors);

            return quadrantVectors.Select(vector => galaxy.Sectors.Single(s => s.Vector == vector));
        }

        private IEnumerable<Sector> GetQuadrantSectors(Sector sectorInQuadrant)
        {
            var quadrant = _galaxy.Quadrants.Single(q => q.SectorVectors.Contains(sectorInQuadrant.Vector));
            return GetQuadrantSectors(quadrant.Vector);
        }

        private IEnumerable<Sector> GetQuadrantSectors(Vector2 quadrantPosition)
        {
            var quadrant = _galaxy.Quadrants.Single(q => q.Vector == quadrantPosition);
            return quadrant.SectorVectors.Select(qs => _galaxy.Sectors.Single(s => s.Vector == qs));
        }
    }
}