﻿using AutoFixture;
using FluentAssertions;
using NUnit.Framework;
using StarTrekTDD.Map.Structures;
using System.Collections.Generic;
using System.Numerics;

namespace StarTrekTDD.Tests.Map.Structures
{
    public class QuadrantTests : TestBase
    {
        [Test]
        public void EqualsMethod_Should_ReturnTrue_If_QuadrantsHaveSameVector()
        {
            var firstQuadrant = Fixture.Create<Quadrant>();
            var secondQuadrant = new Quadrant(firstQuadrant.Vector, Fixture.Create<HashSet<Vector2>>());

            var result = firstQuadrant.Equals(secondQuadrant);

            result.Should().BeTrue();
        }

        [Test]
        public void EqualsMethod_Should_ReturnFalse_If_QuadrantsHaveDifferentVector()
        {
            var firstQuadrant = Fixture.Create<Quadrant>();
            var secondQuadrant = Fixture.Create<Quadrant>();

            var result = firstQuadrant.Equals(secondQuadrant);

            result.Should().BeFalse();
        }

        [Test]
        public void EqualsMethod_Should_ReturnTrue_If_ObjectIsQuadrantType()
        {
            var firstQuadrant = Fixture.Create<Quadrant>();
            var secondQuadrant = (object)new Quadrant(firstQuadrant.Vector, Fixture.Create<HashSet<Vector2>>());

            var result = firstQuadrant.Equals(secondQuadrant);

            result.Should().BeTrue();
        }

        [Test]
        public void EqualsMethod_Should_ReturnFalse_If_ObjectIsNotQuadrantType()
        {
            var firstQuadrant = Fixture.Create<Quadrant>();
            var secondQuadrant = (object)1;

            var result = firstQuadrant.Equals(secondQuadrant);

            result.Should().BeFalse();
        }

        [Test]
        public void EqualsOperator_Should_ReturnTrue_If_QuadrantsHaveSameVector()
        {
            var firstQuadrant = Fixture.Create<Quadrant>();
            var secondQuadrant = new Quadrant(firstQuadrant.Vector, Fixture.Create<HashSet<Vector2>>());

            var result = firstQuadrant == secondQuadrant;

            result.Should().BeTrue();
        }

        [Test]
        public void EqualsOperator_Should_ReturnFalse_If_QuadrantsHaveDifferentVector()
        {
            var firstQuadrant = Fixture.Create<Quadrant>();
            var secondQuadrant = Fixture.Create<Quadrant>();

            var result = firstQuadrant == secondQuadrant;

            result.Should().BeFalse();
        }

        [Test]
        public void NotEqualsOperator_Should_ReturnTrue_If_QuadrantsHaveDifferentVector()
        {
            var firstQuadrant = Fixture.Create<Quadrant>();
            var secondQuadrant = Fixture.Create<Quadrant>();

            var result = firstQuadrant != secondQuadrant;

            result.Should().BeTrue();
        }

        [Test]
        public void NotEqualsOperator_Should_ReturnFalse_If_QuadrantsHaveSameVector()
        {
            var firstQuadrant = Fixture.Create<Quadrant>();
            var secondQuadrant = new Quadrant(firstQuadrant.Vector, Fixture.Create<HashSet<Vector2>>());

            var result = firstQuadrant != secondQuadrant;

            result.Should().BeFalse();
        }
    }
}