﻿using AutoFixture;
using FluentAssertions;
using NUnit.Framework;
using StarTrekTDD.Map.Structures;
using System.Numerics;

namespace StarTrekTDD.Tests.Map.Structures
{
    public class DistanceTests : TestBase
    {
        [TestCase(3, 4)]
        [TestCase(4, 3)]
        [TestCase(1, 0)]
        [TestCase(0, 1)]
        public void MovedSectors_Should_Be_CorrectlyCalculated(int endSectorX, int endSectorY)
        {
            var startSectorPosition = new Vector2(2, 2);
            var endSectorPosition = new Vector2(endSectorX, endSectorY);

            var distance = new Distance(
                startPosition: new Position(startSectorPosition, Fixture.Create<Vector2>()),
                endPosition: new Position(endSectorPosition, Fixture.Create<Vector2>()));

            distance.MovedSectors.Should().Be(3);
        }

        [TestCase(3, 4)]
        [TestCase(4, 3)]
        [TestCase(1, 0)]
        [TestCase(0, 1)]
        public void MovedQuadrants_Should_Be_CorrectlyCalculated(int endQuadrantX, int endQuadrantY)
        {
            var startQuadrantPosition = new Vector2(2, 2);
            var endQuadrantPosition = new Vector2(endQuadrantX, endQuadrantY);

            var distance = new Distance(
                startPosition: new Position(Fixture.Create<Vector2>(), startQuadrantPosition),
                endPosition: new Position(Fixture.Create<Vector2>(), endQuadrantPosition));

            distance.MovedQuadrants.Should().Be(3);
        }

        [TestCase(0, 1, true)]
        [TestCase(1, 1, false)]
        public void ChangedQuadrant_Should_BeTrue_If_MovedQuadrantsNumberIsGreaterThanZero(
            int endQuadrantX, int endQuadrantY, bool expectedResult)
        {
            var startQuadrantPosition = new Vector2(1, 1);
            var endQuadrantPosition = new Vector2(endQuadrantX, endQuadrantY);

            var distance = new Distance(
                startPosition: new Position(Fixture.Create<Vector2>(), startQuadrantPosition),
                endPosition: new Position(Fixture.Create<Vector2>(), endQuadrantPosition));

            distance.ChangedQuadrant.Should().Be(expectedResult);
        }

        [TestCase(0, 1, 0, 0, 1)]
        [TestCase(1, 0, 0, 0, 1)]
        [TestCase(1, 1, 0, 0, 2)]
        public void CalculateCityMetric_Should_Be_CorrectlyCalculated(int x1, int y1, int x2, int y2, int expectedResult)
        {
            var startPosition = new Vector2(x1, y1);
            var endPosition = new Vector2(x2, y2);

            var result = Distance.CalculateCityMetric(startPosition, endPosition);

            result.Should().Be(expectedResult);
        }
    }
}