﻿using AutoFixture;
using FluentAssertions;
using NUnit.Framework;
using StarTrekTDD.Map.Enums;
using StarTrekTDD.Map.Structures;

namespace StarTrekTDD.Tests.Map.Structures
{
    public class SectorTests : TestBase
    {
        [Test]
        public void SpaceObjectAfterChange_Should_BeSetCorrectly()
        {
            var quadrant = Fixture.Create<Sector>();
            var newSpaceObject = Fixture.CreateExcept(quadrant.SpaceObject);

            quadrant.ChangeSpaceObject(newSpaceObject);

            quadrant.SpaceObject.Should().Be(newSpaceObject);
        }

        [Test]
        public void EqualsMethod_Should_ReturnTrue_If_SectorsHaveSameVector()
        {
            var firstSector = Fixture.Create<Sector>();
            var secondSector = new Sector(firstSector.Vector, Fixture.Create<SpaceObjectType>());

            var result = firstSector.Equals(secondSector);

            result.Should().BeTrue();
        }

        [Test]
        public void EqualsMethod_Should_ReturnFalse_If_SectorsHaveDifferentVector()
        {
            var firstSector = Fixture.Create<Sector>();
            var secondSector = Fixture.Create<Sector>();

            var result = firstSector.Equals(secondSector);

            result.Should().BeFalse();
        }

        [Test]
        public void EqualsMethod_Should_ReturnTrue_If_ObjectIsSectorType()
        {
            var firstSector = Fixture.Create<Sector>();
            var secondSector = (object)new Sector(firstSector.Vector, Fixture.Create<SpaceObjectType>());

            var result = firstSector.Equals(secondSector);

            result.Should().BeTrue();
        }

        [Test]
        public void EqualsMethod_Should_ReturnFalse_If_ObjectIsNotSectorType()
        {
            var firstSector = Fixture.Create<Sector>();
            var secondSector = (object)1;

            var result = firstSector.Equals(secondSector);

            result.Should().BeFalse();
        }

        [Test]
        public void EqualsOperator_Should_ReturnTrue_If_SectorsHaveSameVector()
        {
            var firstSector = Fixture.Create<Sector>();
            var secondSector = new Sector(firstSector.Vector, Fixture.Create<SpaceObjectType>());

            var result = firstSector == secondSector;

            result.Should().BeTrue();
        }

        [Test]
        public void EqualsOperator_Should_ReturnFalse_If_SectorsHaveDifferentVector()
        {
            var firstSector = Fixture.Create<Sector>();
            var secondSector = Fixture.Create<Sector>();

            var result = firstSector == secondSector;

            result.Should().BeFalse();
        }

        [Test]
        public void NotEqualsOperator_Should_ReturnTrue_If_SectorsHaveDifferentVector()
        {
            var firstSector = Fixture.Create<Sector>();
            var secondSector = Fixture.Create<Sector>();

            var result = firstSector != secondSector;

            result.Should().BeTrue();
        }

        [Test]
        public void NotEqualsOperator_Should_ReturnFalse_If_SectorsHaveSameVector()
        {
            var firstSector = Fixture.Create<Sector>();
            var secondSector = new Sector(firstSector.Vector, Fixture.Create<SpaceObjectType>());

            var result = firstSector != secondSector;

            result.Should().BeFalse();
        }
    }
}