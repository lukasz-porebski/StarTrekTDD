﻿using AutoFixture;
using System.Linq;

namespace StarTrekTDD.Tests
{
    public static class FixtureExtensions
    {
        public static T CreateExcept<T>(this IFixture fixture, T exceptElement) =>
            fixture.Create<Generator<T>>().First(s => !exceptElement.Equals(s));
    }
}