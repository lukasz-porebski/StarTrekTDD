﻿using FluentAssertions;
using NUnit.Framework;
using StarTrekTDD.Game.Ships;
using System;

namespace StarTrekTDD.Tests.Game.Ships
{
    public class KlingonShipTests : TestBase
    {
        private KlingonShip _klingonShip;

        [SetUp]
        public void Setup()
        {
            _klingonShip = new KlingonShip(Guid.NewGuid());
        }

        [Test]
        public void EnergyUnitsNumber_Should_BeSameAs_InGameInitConfig()
        {
            _klingonShip.EnergyUnits.Should().Be(InitGameConfig.KlingonShipEnergyUnits);
        }

        [Test]
        public void HitValue_Should_BeGreaterThan_Zero()
        {
            var hitValue = _klingonShip.Hit();

            hitValue.Should().BeGreaterThan(0);
        }

        [Test]
        public void HitValue_Should_NotBeLessOrEqualTo_EnergyUnitsValue()
        {
            var energyUnits = _klingonShip.EnergyUnits;

            var hitValue = _klingonShip.Hit();

            hitValue.Should().BeLessOrEqualTo(energyUnits);
        }
    }
}