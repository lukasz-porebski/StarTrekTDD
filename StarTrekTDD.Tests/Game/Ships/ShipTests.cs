﻿using FluentAssertions;
using NUnit.Framework;
using StarTrekTDD.Game.Ships;

namespace StarTrekTDD.Tests.Game.Ships
{
    public class ShipTests : TestBase
    {
        [Test]
        public void EnergyUnitsAfterReduce_Should_Be_ProperlyCalculated()
        {
            var ship = new Ship(5);

            ship.ReduceEnergyUnits(4);

            ship.EnergyUnits.Should().Be(1);
        }

        [TestCase(-1, false)]
        [TestCase(-1, false)]
        [TestCase(1, true)]
        public void HasEnergyUnits_Should_ReturnTrue_If_IsNoEnergyUnit(int energyUnits, bool expectedResult)
        {
            var ship = new Ship(energyUnits);

            ship.HasEnergyUnits.Should().Be(expectedResult);
        }

        [Test]
        public void HasEnergyUnits_Should_ReturnTrue_If_IsEnergyUnit()
        {
            var ship = new Ship(5);

            ship.HasEnergyUnits.Should().BeTrue();
        }
    }
}