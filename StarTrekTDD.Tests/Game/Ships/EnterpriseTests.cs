﻿using AutoFixture;
using FluentAssertions;
using NUnit.Framework;
using StarTrekTDD.Game.Ships;
using StarTrekTDD.Map.Structures;
using System.Numerics;

namespace StarTrekTDD.Tests.Game.Ships
{
    public class EnterpriseTests : TestBase
    {
        private Enterprise _enterprise;

        [SetUp]
        public void Setup()
        {
            _enterprise = new Enterprise();
        }

        [Test]
        public void EnergyUnitsNumber_Should_BeSameAs_InGameInitConfig()
        {
            _enterprise.EnergyUnits.Should().Be(InitGameConfig.EnergyUnits);
        }

        [Test]
        public void StellarDatesNumber_Should_BeSameAs_InGameInitConfig()
        {
            _enterprise.StellarDates.Should().Be(InitGameConfig.StellarDates);
        }

        [Test]
        public void ResourcesAfterReduce_Should_Be_ProperlyCalculated()
        {
            var startPosition = new Position(new Vector2(1, 1), new Vector2(1, 1));
            var endPosition = new Position(new Vector2(1, 2), new Vector2(2, 1));

            _enterprise.ReduceResources(new Distance(startPosition, endPosition));

            _enterprise.EnergyUnits.Should().Be(InitGameConfig.EnergyUnits - 1);
            _enterprise.StellarDates.Should().Be(InitGameConfig.StellarDates - 1);
        }

        [Test]
        public void EnergyUnitsAfterReset_Should_BeSameAs_InGameInitConfig()
        {
            _enterprise.ReduceResources(Fixture.Create<Distance>());

            _enterprise.ResetEnergyUnits();

            _enterprise.EnergyUnits.Should().Be(InitGameConfig.EnergyUnits);
        }

        [Test]
        public void HasStellarDate_Should_ReturnFalse_If_ThereIsNoStellarDate()
        {
            var startPosition = new Position(Fixture.Create<Vector2>(), new Vector2(0, 0));
            var endPosition = new Position(Fixture.Create<Vector2>(), new Vector2(65, 65));
            _enterprise.ReduceResources(new Distance(startPosition, endPosition));

            _enterprise.HasStellarDate.Should().BeFalse();
        }

        [Test]
        public void HasStellarDate_Should_ReturnTrue_If_IsStellarDate()
        {
            _enterprise.HasStellarDate.Should().BeTrue();
        }

        [Test]
        public void TryReduceEnergyUnits_Should_ReturnTrue_If_CanReduce()
        {
            var result = _enterprise.TryReduceEnergyUnits(1);

            result.Should().BeTrue();
        }

        [Test]
        public void TryReduceEnergyUnits_Should_ReturnFalse_If_CannotReduce()
        {
            var energyUnitsToReduce = _enterprise.EnergyUnits + 1;

            var result = _enterprise.TryReduceEnergyUnits(energyUnitsToReduce);

            result.Should().BeFalse();
        }

        [Test]
        public void EnergyUnits_After_SuccessTryReduceEnergyUnits_Should_Be_Reduced()
        {
            var currentEnergyUnits = _enterprise.EnergyUnits;

            _enterprise.TryReduceEnergyUnits(1);

            _enterprise.EnergyUnits.Should().Be(currentEnergyUnits - 1);
        }

        [Test]
        public void EnergyUnits_After_FailTryReduceEnergyUnits_Should_NotBe_Reduced()
        {
            var currentEnergyUnits = _enterprise.EnergyUnits;

            _enterprise.TryReduceEnergyUnits(currentEnergyUnits + 1);

            _enterprise.EnergyUnits.Should().Be(currentEnergyUnits);
        }
    }
}