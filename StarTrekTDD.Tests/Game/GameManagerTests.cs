﻿using AutoFixture;
using FluentAssertions;
using NUnit.Framework;
using StarTrekTDD.Game;
using StarTrekTDD.Game.Enums;
using StarTrekTDD.Map.Enums;
using System.Linq;
using System.Numerics;

namespace StarTrekTDD.Tests.Game
{
    public class GameManagerTests : TestBase
    {
        private GameManager _gameManager;

        [SetUp]
        public void SetUp()
        {
            _gameManager = Fixture.Create<GameManager>();
        }

        [Test]
        public void EnterpriseAfterMove_Should_HasCorrectlyVector()
        {
            var previousEnterpriseSector = _gameManager.Galaxy.EnterpriseSector;
            var moveDistance = new Vector2(1, 1);

            _gameManager.MoveEnterprise(moveDistance);

            _gameManager.Galaxy.EnterpriseSector.Vector
                .Should()
                .Be(previousEnterpriseSector.Vector + moveDistance);
        }

        [Test]
        public void EnterpriseAfterMove_Should_HasReducedResources()
        {
            var previousEnterpriseSector = _gameManager.Galaxy.EnterpriseSector;
            var moveDistance = new Vector2(1, 1);
            var distanceDetails = _gameManager.Galaxy.CalculateDistance(previousEnterpriseSector.Vector, moveDistance);
            var currentEnergyUnits = _gameManager.Enterprise.EnergyUnits;
            var currentStellarDates = _gameManager.Enterprise.StellarDates;

            _gameManager.MoveEnterprise(moveDistance);

            _gameManager.Enterprise.EnergyUnits.Should().Be(currentEnergyUnits - distanceDetails.MovedSectors);
            _gameManager.Enterprise.StellarDates.Should().Be(currentStellarDates - distanceDetails.MovedQuadrants);
        }

        [Test]
        public void NewQuadrant_AfterEnterpriseMove_Should_Be_Shuffled()
        {
            var enterpriseQuadrant = _gameManager.Galaxy.GetEnterprisePositionQuadrant();
            var nextToQuadrantPosition = enterpriseQuadrant.Vector + Vector2.One;
            var nextToQuadrant = _gameManager.Galaxy.Quadrants.Single(q => q.Vector == nextToQuadrantPosition);
            var distance = nextToQuadrant.SectorVectors.First() - _gameManager.Galaxy.EnterpriseSector.Vector;
            var currentNextToQuadrantSectorSpaceObjects = _gameManager.Galaxy.Sectors
                .Where(s => nextToQuadrant.SectorVectors.Contains(s.Vector))
                .Select(s => s.SpaceObject)
                .ToList();

            _gameManager.MoveEnterprise(distance);

            var shuffledNextToQuadrantSectorSpaceObjects = _gameManager.Galaxy.Sectors
                .Where(s => nextToQuadrant.SectorVectors.Contains(s.Vector))
                .Select(s => s.SpaceObject)
                .ToList();
            shuffledNextToQuadrantSectorSpaceObjects.Should().NotBeSameAs(currentNextToQuadrantSectorSpaceObjects);
        }

        [Test]
        public void ResultOf_EnterpriseMoveToOccupiedSector_Should_ContainsMovedToOccupiedSectorFlag()
        {
            var starSector = _gameManager.Galaxy.StarSectors.First();
            MoveEnterpriseToEmptyQuadrantSector(_gameManager.Galaxy, starSector);
            var moveDistance = starSector.Vector - _gameManager.Galaxy.EnterpriseSector.Vector;

            var result = _gameManager.MoveEnterprise(moveDistance);

            result.Should().HaveFlag(CommandResult.MovedToOccupiedSector);
        }

        [Test]
        public void ResultOf_EnterpriseMoveToKlingonShipsQuadrant_Should_ContainsKlingonShipsAttackedFlag()
        {
            var previousEnterpriseSector = _gameManager.Galaxy.EnterpriseSector;
            var klingonShipSector = _gameManager.Galaxy.KlingonShipSectors.First();
            var klingonShipQuadrant = _gameManager.Galaxy.Quadrants
                .Single(q => q.SectorVectors.Contains(klingonShipSector.Vector));
            var emptySector = _gameManager.Galaxy.Sectors.First(s =>
                klingonShipQuadrant.SectorVectors.Contains(s.Vector) &&
                s.SpaceObject.Type == SpaceObjectType.Emptiness);
            var moveDistance = emptySector.Vector - previousEnterpriseSector.Vector;

            var result = _gameManager.MoveEnterprise(moveDistance);

            result.Should().HaveFlag(CommandResult.KlingonShipsAttacked);
        }

        [Test]
        public void ResultOf_EnterpriseMoveToSectorNextToStarFleetBase_Should_ContainsEnergyUnitsRenewedFlag()
        {
            var starSector = _gameManager.Galaxy.StarFleetBaseSectors.First();
            MoveEnterpriseToEmptyQuadrantSector(_gameManager.Galaxy, starSector);
            var moveDistance = starSector.Vector - _gameManager.Galaxy.EnterpriseSector.Vector - new Vector2(1, 0);

            var result = _gameManager.MoveEnterprise(moveDistance);

            result.Should().HaveFlag(CommandResult.EnergyUnitsRenewed);
        }

        [Test]
        public void EnterpriseAfterMoveToSectorNextToStarFleetBase_Should_HasRenewedEnergyUnits()
        {
            var starSector = _gameManager.Galaxy.StarFleetBaseSectors.First();
            MoveEnterpriseToEmptyQuadrantSector(_gameManager.Galaxy, starSector);
            var moveDistance = starSector.Vector - _gameManager.Galaxy.EnterpriseSector.Vector - new Vector2(1, 0);

            _gameManager.MoveEnterprise(moveDistance);

            _gameManager.Enterprise.EnergyUnits.Should().Be(InitGameConfig.EnergyUnits);
        }

        [Test]
        public void DisplayQuadrantSurroundings_Should_IncrementQuadrantSurroundingsInfosHistory()
        {
            var previousCount = _gameManager.QuadrantSurroundingsInfosHistory.Count;

            _gameManager.DisplayQuadrantSurroundings();

            _gameManager.QuadrantSurroundingsInfosHistory.Count.Should().Be(previousCount + 1);
        }

        [Test]
        public void DisplayGalaxyMap_Should_IncrementQuadrantSurroundingsInfosHistory()
        {
            var previousCount = _gameManager.QuadrantSurroundingsInfosHistory.Count;

            _gameManager.DisplayGalaxyMap();

            _gameManager.QuadrantSurroundingsInfosHistory.Count.Should().Be(previousCount + 1);
        }

        [TestCase(0)]
        [TestCase(1)]
        public void ExistingKlingonShips_Should_AttackAfterEnterpriseAttack(int energyUnitsToUse)
        {
            MoveEnterpriseToEmptyQuadrantSector(_gameManager.Galaxy, _gameManager.Galaxy.KlingonShipSectors.First());

            var result = _gameManager.Fight(energyUnitsToUse);

            result.Should().HaveFlag(CommandResult.KlingonShipsAttacked);
        }

        [Test]
        public void If_EnergyUnitsToUseIsBiggerThanEnterpriseEnergyUnits_Than_Result_Should_ContainsEnergyUnitHasRunOutFlag()
        {
            var result = _gameManager.Fight(_gameManager.Enterprise.EnergyUnits + 1);

            result.Should().HaveFlag(CommandResult.EnergyUnitHasRunOut);
        }

        [Test]
        public void AfterEnterpriseHit_Result_Should_ContainsHitFlag()
        {
            var result = _gameManager.Fight(_gameManager.Enterprise.EnergyUnits - 1);

            var containsHitFlag = result.HasFlag(CommandResult.FailHit) || result.HasFlag(CommandResult.SuccessfulHit);
            containsHitFlag.Should().BeTrue();
        }

        [Test]
        public void ResultOf_EnterpriseMoveToNotExistingSector_Should_ContainsSectorNotExistsFlag()
        {
            var result = _gameManager.MoveEnterprise(new Vector2(64, 64));

            result.Should().HaveFlag(CommandResult.SectorNotExists);
        }

        [Test]
        public void If_DestroyedAllKlingonShips_ThanResult_Should_ContainsDestroyedAllKlingonShipsFlag()
        {
            RemoveKlingonShips(_gameManager.Galaxy);

            var result = _gameManager.Fight(0);

            result.Should().HaveFlag(CommandResult.DestroyedAllKlingonShips);
        }
    }
}