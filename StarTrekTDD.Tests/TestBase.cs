﻿using AutoFixture;
using StarTrekTDD.Game;
using StarTrekTDD.Game.Ships;
using StarTrekTDD.Map;
using StarTrekTDD.Map.Enums;
using StarTrekTDD.Map.Structures;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace StarTrekTDD.Tests
{
    public abstract class TestBase
    {
        protected readonly IFixture Fixture;

        protected TestBase()
        {
            Fixture = new Fixture();

            Init();
        }

        protected void MoveEnterpriseToEmptyQuadrantSector(Galaxy galaxy, Sector someSectorInQuadrant)
        {
            var quadrant = galaxy.Quadrants.Single(q => q.SectorVectors.Contains(someSectorInQuadrant.Vector));
            var emptySector = GetFirstEmptySector(galaxy, quadrant);
            galaxy.MoveEnterprise(emptySector.Vector);
        }

        protected Sector GetFirstEmptySector(Galaxy galaxy, Quadrant quadrant) =>
            galaxy.Sectors.First(s =>
                quadrant.SectorVectors.Contains(s.Vector) &&
                s.SpaceObject.Type == SpaceObjectType.Emptiness);

        protected static void RemoveKlingonShips(Galaxy galaxy)
        {
            var klingonShipPositions = galaxy.KlingonShipSectors.ToList();

            foreach (var galaxyKlingonShipPosition in klingonShipPositions)
                galaxy.UpdateSectorSpaceObject(galaxyKlingonShipPosition, new SpaceObject(SpaceObjectType.Emptiness));
        }

        private void Init()
        {
            Fixture.Register(() => new GameManager(new Galaxy(), new Enterprise()));
            Fixture.Register(() => new Quadrant(Fixture.Create<Vector2>(), Fixture.Create<HashSet<Vector2>>()));
            Fixture.Register(() => new Sector(Fixture.Create<Vector2>(), Fixture.Create<SpaceObjectType>()));
            Fixture.Register(() => new Position(Fixture.Create<Vector2>(), Fixture.Create<Vector2>()));
            Fixture.Register(() => new Distance(Fixture.Create<Position>(), Fixture.Create<Position>()));
        }
    }
}