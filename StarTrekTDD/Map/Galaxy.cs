﻿using MoreLinq;
using StarTrekTDD.Map.Enums;
using StarTrekTDD.Map.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace StarTrekTDD.Map
{
    public class Galaxy
    {
        private readonly List<Sector> _sectors;
        public IEnumerable<Sector> Sectors => _sectors;

        private readonly List<Quadrant> _quadrants;
        public IEnumerable<Quadrant> Quadrants => _quadrants;

        public IEnumerable<Sector> KlingonShipSectors => Sectors
            .Where(s => s.SpaceObject.Type == SpaceObjectType.KlingonShip);

        public IEnumerable<Sector> StarFleetBaseSectors => Sectors
            .Where(s => s.SpaceObject.Type == SpaceObjectType.StarFleetBase);

        public IEnumerable<Sector> StarSectors => Sectors
            .Where(s => s.SpaceObject.Type == SpaceObjectType.Star);

        public Sector EnterpriseSector => Sectors
            .Single(s => s.SpaceObject.Type == SpaceObjectType.Enterprise);

        public Galaxy()
        {
            _sectors = new List<Sector>();
            _quadrants = new List<Quadrant>();

            const int centerPoint = InitGameConfig.QuadrantsNumberInGalaxy / 2 - 1;
            var spaceObjects = GetShuffledSpaceObjectsExceptEnterprise();

            for (var rowNumber = 0; rowNumber < InitGameConfig.GridSideSize; rowNumber++)
            {
                for (var columnNumber = 0; columnNumber < InitGameConfig.GridSideSize; columnNumber++)
                {
                    var quadrant = CreateQuadrant(rowNumber, columnNumber);
                    _quadrants.Add(new Quadrant(new Vector2(rowNumber, columnNumber), quadrant));

                    foreach (var sectorVector in quadrant)
                    {
                        var spaceObject = (sectorVector.X == centerPoint && sectorVector.Y == centerPoint)
                            ? SpaceObjectType.Enterprise
                            : spaceObjects.Pop();

                        _sectors.Add(new Sector(sectorVector, spaceObject));
                    }
                }
            }
        }

        public void ShuffleQuadrant(Vector2 quadrantPosition)
        {
            var sectors = GetQuadrantSectors(quadrantPosition);
            var shuffledSpaceObjects = new Stack<SpaceObject>(sectors.Select(s => s.SpaceObject).Shuffle());

            foreach (var sector in sectors)
            {
                var positionIndex = _sectors.FindIndex(s => s == sector);
                sector.ChangeSpaceObject(shuffledSpaceObjects.Pop());
                _sectors[positionIndex] = sector;
            }
        }

        public Distance CalculateDistance(Vector2 startPosition, Vector2 move)
        {
            var endPosition = startPosition + move;

            var startPositionQuadrant = Quadrants.Single(q => q.SectorVectors.Contains(startPosition));
            var endPositionQuadrant = Quadrants.Single(q => q.SectorVectors.Contains(endPosition));

            return new Distance(
                startPosition: new Position(startPosition, startPositionQuadrant.Vector),
                endPosition: new Position(endPosition, endPositionQuadrant.Vector));
        }

        public bool IsAnyKlingonShipInsideEnterpriseQuadrant()
        {
            if (!KlingonShipSectors.Any())
                return false;

            var quadrant = Quadrants.Single(q => q.SectorVectors.Contains(EnterpriseSector.Vector));

            return quadrant.SectorVectors.Any(s => KlingonShipSectors.Select(ks => ks.Vector).Contains(s));
        }

        public bool IsAnyKlingonShipOutsideEnterpriseQuadrant()
        {
            if (KlingonShipSectors.Any())
                return false;

            var quadrant = GetEnterprisePositionQuadrant();
            var quadrantOutsideTargetQuadrant = Quadrants.Except(new HashSet<Quadrant>
            {
                quadrant
            });
            var quadrantOutsideTargetQuadrantWithKlingonShip = quadrantOutsideTargetQuadrant
                .Where(q => q.SectorVectors.Any(s => KlingonShipSectors.Any(k => k.Vector == s)));

            return quadrantOutsideTargetQuadrantWithKlingonShip.Contains(quadrant);
        }

        public bool IsEnterpriseNextToStarFleetBase()
        {
            if (!StarFleetBaseSectors.Any())
                return false;

            var leftSector = Sectors.SingleOrDefault(s => s.Vector == EnterpriseSector.Vector + new Vector2(-1, 0));
            var rightSector = Sectors.SingleOrDefault(s => s.Vector == EnterpriseSector.Vector + new Vector2(1, 0));
            var upSector = Sectors.SingleOrDefault(s => s.Vector == EnterpriseSector.Vector + new Vector2(0, 1));
            var downSector = Sectors.SingleOrDefault(s => s.Vector == EnterpriseSector.Vector + new Vector2(0, -1));

            var nextToSectors = new List<Sector>
            {
                leftSector, rightSector, upSector, downSector
            };

            return nextToSectors.Any(s => s.SpaceObject.Type == SpaceObjectType.StarFleetBase);
        }

        public void UpdateSectorSpaceObject(Sector sector, SpaceObject newSpaceObject)
        {
            var index = _sectors.IndexOf(sector);
            sector.ChangeSpaceObject(newSpaceObject);
            _sectors[index] = sector;
        }

        public bool IsSectorEmpty(Vector2 sectorPosition) =>
            Sectors.Single(s => s.Vector == sectorPosition).SpaceObject.Type == SpaceObjectType.Emptiness;

        public void MoveEnterprise(Vector2 targetPosition)
        {
            var currentEnterpriseSector = EnterpriseSector;
            var targetPositionSector = _sectors.Single(s => s.Vector == targetPosition);

            UpdateSectorSpaceObject(targetPositionSector, currentEnterpriseSector.SpaceObject);
            UpdateSectorSpaceObject(currentEnterpriseSector, new SpaceObject(SpaceObjectType.Emptiness));
        }

        public Quadrant GetEnterprisePositionQuadrant() =>
            Quadrants.Single(q => q.SectorVectors.Any(s => s == EnterpriseSector.Vector));

        public Sector GetEnterpriseSectorClosetKlingonShipSector()
        {
            var result = new Sector();
            var closetSectorsDistance = int.MaxValue;

            foreach (var klingonShipSector in KlingonShipSectors)
            {
                var move = klingonShipSector.Vector - EnterpriseSector.Vector;
                var distance = CalculateDistance(EnterpriseSector.Vector, move);

                if (distance.MovedSectors < closetSectorsDistance)
                {
                    result = klingonShipSector;
                    closetSectorsDistance = distance.MovedSectors;
                }
            }

            return result;
        }

        public QuadrantSurroundingsInfo GetEnterpriseQuadrantSurroundingsInfo()
        {
            var enterprisePositionQuadrant = GetEnterprisePositionQuadrant();
            var quadrantSurroundings = GetQuadrantSurroundings(enterprisePositionQuadrant);
            var sectors = GetSectors(quadrantSurroundings);

            var klingonShipsNumber = sectors.Count(s => s.SpaceObject.Type == SpaceObjectType.KlingonShip);
            var starFleetBasesNumber = sectors.Count(s => s.SpaceObject.Type == SpaceObjectType.StarFleetBase);
            var starsNumber = sectors.Count(s => s.SpaceObject.Type == SpaceObjectType.Star);

            return new QuadrantSurroundingsInfo(klingonShipsNumber, starFleetBasesNumber, starsNumber);
        }

        public IEnumerable<Sector> GetKlingonShipSectorsInEnterpriseQuadrant()
        {
            var enterprisePositionQuadrant = GetEnterprisePositionQuadrant();
            return KlingonShipSectors.Where(ks => enterprisePositionQuadrant.SectorVectors.Contains(ks.Vector));
        }

        public bool SectorExists(Vector2 sectorPosition) =>
            Sectors.Any(s => s.Vector == sectorPosition);

        private static HashSet<Vector2> CreateQuadrant(int rowNumber, int columnNumber)
        {
            var result = new HashSet<Vector2>();
            const int gridSideSize = InitGameConfig.GridSideSize;

            var xStartPosition = rowNumber * gridSideSize;
            var xEndPosition = rowNumber * gridSideSize + gridSideSize;

            var yStartPosition = columnNumber * gridSideSize;
            var yEndPosition = columnNumber * gridSideSize + gridSideSize;


            for (var x = xStartPosition; x < xEndPosition; x++)
            {
                for (var y = yStartPosition; y < yEndPosition; y++)
                {
                    result.Add(new Vector2(x, y));
                }
            }

            return result;
        }

        private static Stack<SpaceObjectType> GetShuffledSpaceObjectsExceptEnterprise()
        {
            var spaceObjects = new List<SpaceObjectType>();

            var klingonShips = Enumerable.Repeat(
                SpaceObjectType.KlingonShip, InitGameConfig.KlingonShipsNumberInGalaxy);
            spaceObjects.AddRange(klingonShips);

            var starFleetBases = Enumerable.Repeat(
                SpaceObjectType.StarFleetBase, InitGameConfig.StarFleetBasesNumberInGalaxy);
            spaceObjects.AddRange(starFleetBases);

            var stars = Enumerable.Repeat(
                SpaceObjectType.Star, InitGameConfig.StarsNumberInGalaxy);
            spaceObjects.AddRange(stars);

            var emptiness = Enumerable.Repeat(
                SpaceObjectType.Emptiness,
                InitGameConfig.SectorsNumberInQuadrant * InitGameConfig.QuadrantsNumberInGalaxy - spaceObjects.Count);
            spaceObjects.AddRange(emptiness);


            var random = new Random();
            var shuffledSpaceObjects = spaceObjects.OrderBy(c => random.Next());

            var result = new Stack<SpaceObjectType>();

            foreach (var spaceObject in shuffledSpaceObjects)
                result.Push(spaceObject);

            return result;
        }

        private IEnumerable<Quadrant> GetQuadrantSurroundings(Quadrant quadrant)
        {
            var result = new List<Quadrant>();
            var surroundings = new List<int> { -1, 0, 1 };
            result.Add(quadrant);

            foreach (var sur1 in surroundings)
            {
                foreach (var sur2 in surroundings)
                {
                    var move = new Vector2(sur1, sur2);
                    var targetQuadrantPosition = quadrant.Vector + move;

                    var quadrantSurrounding = Quadrants.SingleOrDefault(q => q.Vector == targetQuadrantPosition);

                    if (quadrantSurrounding.SectorVectors.Any())
                        result.Add(quadrantSurrounding);
                }
            }

            return result;
        }

        private IEnumerable<Sector> GetSectors(IEnumerable<Quadrant> quadrants)
        {
            var quadrantVectors = quadrants.SelectMany(q => q.SectorVectors);

            return quadrantVectors.Select(vector => Sectors.Single(s => s.Vector == vector));
        }

        private IEnumerable<Sector> GetQuadrantSectors(Vector2 quadrantPosition)
        {
            var quadrant = _quadrants.Single(q => q.Vector == quadrantPosition);
            return quadrant.SectorVectors.Select(qs => _sectors.Single(s => s.Vector == qs));
        }
    }
}