﻿namespace StarTrekTDD.Map.Structures
{
    public readonly struct QuadrantSurroundingsInfo
    {
        public int KlingonShipsNumber { get; }
        public int StarFleetBasesNumber { get; }
        public int StarsNumber { get; }

        public QuadrantSurroundingsInfo(int klingonShipsNumber, int starFleetBasesNumber, int starsNumber)
        {
            KlingonShipsNumber = klingonShipsNumber;
            StarFleetBasesNumber = starFleetBasesNumber;
            StarsNumber = starsNumber;
        }
    }
}