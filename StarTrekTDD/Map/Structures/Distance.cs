﻿using System;
using System.Numerics;

namespace StarTrekTDD.Map.Structures
{
    public readonly struct Distance
    {
        public Position StartPosition { get; }
        public Position EndPosition { get; }
        public int MovedQuadrants { get; }
        public int MovedSectors { get; }
        public bool ChangedQuadrant => MovedQuadrants > 0;

        public Distance(Position startPosition, Position endPosition)
        {
            StartPosition = startPosition;
            EndPosition = endPosition;

            var distanceBetweenQuadrants = startPosition.Quadrant - endPosition.Quadrant;
            var distanceBetweenSectors = startPosition.Sector - endPosition.Sector;

            MovedQuadrants = (int)(Math.Abs(distanceBetweenQuadrants.X) + Math.Abs(distanceBetweenQuadrants.Y));
            MovedSectors = (int)(Math.Abs(distanceBetweenSectors.X) + Math.Abs(distanceBetweenSectors.Y));
        }

        public static int CalculateCityMetric(Vector2 startPosition, Vector2 endPosition)
        {
            var distance = startPosition - endPosition;
            return (int)(Math.Abs(distance.X) + Math.Abs(distance.Y));
        }
    }
}