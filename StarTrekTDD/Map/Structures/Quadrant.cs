﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace StarTrekTDD.Map.Structures
{
    public struct Quadrant : IEquatable<Quadrant>
    {
        public Vector2 Vector { get; }
        public ISet<Vector2> SectorVectors { get; private set; }

        public Quadrant(Vector2 vector, ISet<Vector2> sectorVectors)
        {
            Vector = vector;
            SectorVectors = sectorVectors;
        }

        public bool Equals(Quadrant other)
        {
            return Vector.Equals(other.Vector);
        }

        public override bool Equals(object obj)
        {
            return obj is Quadrant other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Vector.GetHashCode();
        }

        public static bool operator ==(Quadrant left, Quadrant right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Quadrant left, Quadrant right)
        {
            return !left.Equals(right);
        }
    }
}