﻿using StarTrekTDD.Map.Enums;
using System;

namespace StarTrekTDD.Map.Structures
{
    public readonly struct SpaceObject
    {
        public Guid Id { get; }
        public SpaceObjectType Type { get; }

        public SpaceObject(SpaceObjectType type)
        {
            Id = Guid.NewGuid();
            Type = type;
        }
    }
}