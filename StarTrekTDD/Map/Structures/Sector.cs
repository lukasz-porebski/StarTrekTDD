﻿using StarTrekTDD.Map.Enums;
using System;
using System.Numerics;

namespace StarTrekTDD.Map.Structures
{
    public struct Sector : IEquatable<Sector>
    {
        public Vector2 Vector { get; }
        public SpaceObject SpaceObject { get; private set; }

        public Sector(Vector2 vector, SpaceObjectType spaceObject)
        {
            Vector = vector;
            SpaceObject = new SpaceObject(spaceObject);
        }

        public void ChangeSpaceObject(SpaceObject newSpaceObject)
        {
            SpaceObject = newSpaceObject;
        }

        public bool Equals(Sector other)
        {
            return Vector.Equals(other.Vector);
        }

        public override bool Equals(object obj)
        {
            return obj is Sector other && Equals(other);
        }

        public override int GetHashCode()
        {
            return Vector.GetHashCode();
        }

        public static bool operator ==(Sector left, Sector right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Sector left, Sector right)
        {
            return !left.Equals(right);
        }
    }
}