﻿using System.Numerics;

namespace StarTrekTDD.Map.Structures
{
    public readonly struct Position
    {
        public Vector2 Sector { get; }
        public Vector2 Quadrant { get; }

        public Position(Vector2 sector, Vector2 quadrant)
        {
            Sector = sector;
            Quadrant = quadrant;
        }
    }
}