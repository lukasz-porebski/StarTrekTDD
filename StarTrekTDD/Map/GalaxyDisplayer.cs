﻿using StarTrekTDD.Map.Enums;
using StarTrekTDD.Map.Structures;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StarTrekTDD.Map
{
    public class GalaxyDisplayer
    {
        private readonly Galaxy _galaxy;

        public GalaxyDisplayer(Galaxy galaxy)
        {
            _galaxy = galaxy;
        }

        public void DisplayEnterpriseQuadrant()
        {
            var enterprisePositionQuadrant = _galaxy.GetEnterprisePositionQuadrant();
            var spaceObjects = GetQuadrantSpaceObjectTypes(enterprisePositionQuadrant);

            foreach (var rowSpaceObjects in spaceObjects)
            {
                foreach (var spaceObject in rowSpaceObjects)
                {
                    Console.Write(GetSpaceObjectSymbol(spaceObject) + "\t");
                }

                Console.WriteLine();
            }
        }


        public void DisplayEnterpriseQuadrantSurroundings(QuadrantSurroundingsInfo info)
        {
            Console.WriteLine("K\tB\tS\t");
            Console.WriteLine($"{info.KlingonShipsNumber}\t{info.StarFleetBasesNumber}\t{info.StarsNumber}");
        }

        private IEnumerable<List<SpaceObjectType>> GetQuadrantSpaceObjectTypes(Quadrant quadrant)
        {
            var rows = new Stack<List<SpaceObjectType>>();
            var currentGalaxyRowIndex = quadrant.SectorVectors.First().Y;
            rows.Push(new List<SpaceObjectType>());
            var orderedSectors = quadrant.SectorVectors.OrderBy(s => s.Y).ThenBy(s => s.X);

            foreach (var vector in orderedSectors)
            {
                var sector = _galaxy.Sectors.Single(s => s.Vector == vector);

                if (vector.Y != currentGalaxyRowIndex)
                {
                    ++currentGalaxyRowIndex;
                    rows.Push(new List<SpaceObjectType>());
                }

                rows.Peek().Add(sector.SpaceObject.Type);
            }

            return rows.ToList();
        }

        private static string GetSpaceObjectSymbol(SpaceObjectType spaceObject) =>
            spaceObject switch
            {
                SpaceObjectType.Emptiness => " . ",
                SpaceObjectType.KlingonShip => "***",
                SpaceObjectType.StarFleetBase => ">!<",
                SpaceObjectType.Star => " * ",
                SpaceObjectType.Enterprise => "<*>",
                _ => throw new ArgumentOutOfRangeException(nameof(spaceObject), spaceObject, null)
            };
    }
}