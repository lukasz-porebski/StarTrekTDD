﻿namespace StarTrekTDD.Map.Enums
{
    public enum SpaceObjectType
    {
        Emptiness = 1,
        KlingonShip,
        StarFleetBase,
        Star,
        Enterprise
    }
}