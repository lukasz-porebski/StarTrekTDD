﻿using System;

namespace StarTrekTDD.Game.Enums
{
    [Flags]
    public enum CommandResult
    {
        None = 0,
        KlingonShipsAttacked = 1,
        Moved = 2,
        EnergyUnitHasRunOut = 4,
        StellarDateHasRunOut = 8,
        MovedToOccupiedSector = 16,
        EnergyUnitsRenewed = 32,
        SuccessfulHit = 64,
        FailHit = 128,
        SectorNotExists = 256,
        DestroyedAllKlingonShips = 512
    }
}