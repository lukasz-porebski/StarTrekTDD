﻿namespace StarTrekTDD.Game.Enums
{
    public enum Command
    {
        MoveEnterprise = 1,
        ShortScan,
        LongScan,
        Battle,
        GalaxyMap,
        Incorrect
    }
}