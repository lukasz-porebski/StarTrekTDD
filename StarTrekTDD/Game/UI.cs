﻿using StarTrekTDD.Game.Enums;
using System;
using System.Numerics;

namespace StarTrekTDD.Game
{
    public class UI
    {
        public void ShowEnterpriseStatus(GameManager gameManager)
        {
            Console.WriteLine("Status Enterprise");
            Console.WriteLine($"Ilość zapasów energii: {gameManager.Enterprise.EnergyUnits}");
            Console.WriteLine($"Ilość dat gwiezdnych: {gameManager.Enterprise.StellarDates}");
            Console.WriteLine($"Pozycja: {gameManager.Galaxy.EnterpriseSector.Vector}");
        }

        public Command GetCorrectCommand()
        {
            Console.Write("Wprowadź komendę: ");
            do
            {
                var value = Console.ReadLine();

                if (int.TryParse(value, out var parsedValue))
                {
                    var command = ToCommand(parsedValue);

                    if (command != Command.Incorrect)
                        return command;
                }

                Console.Write("Podana komenda jest nieprawidłowa. Wprowadź poprawną komendę: ");
            } while (true);
        }

        public Vector2 GetVector()
        {
            var wrongVectorMessage = "Podany vector jest nieprawidłowy. Wprowadź poprawny vector: ";
            Console.Write("VECTOR? ");
            do
            {
                var value = Console.ReadLine();
                var splitValue = value.Split(",");

                if (splitValue.Length != 2)
                    Console.Write(wrongVectorMessage);
                else
                {
                    var xParseResult = int.TryParse(splitValue[0], out var x);
                    var yParseResult = int.TryParse(splitValue[1], out var y);

                    var isSameSector = x == 0 && y == 0;

                    if (xParseResult && yParseResult && !isSameSector)
                        return new Vector2(x, y);

                    Console.Write(wrongVectorMessage);
                }
            } while (true);
        }

        public int GetEnergyUnitsToUse()
        {
            Console.Write("Wprowadź liczbę jednostek energii do użycia: ");
            do
            {
                var value = Console.ReadLine();

                if (int.TryParse(value, out var parsedValue))
                {
                    if (parsedValue >= 0)
                        return parsedValue;
                }

                Console.Write("Liczba jednostek energii do użycia nie może być ujemna. Podaj prawidłową: ");
            } while (true);
        }

        private static Command ToCommand(int value) =>
            value switch
            {
                0 => Command.MoveEnterprise,
                1 => Command.ShortScan,
                2 => Command.LongScan,
                3 => Command.Battle,
                4 => Command.GalaxyMap,
                _ => Command.Incorrect
            };
    }
}