﻿using System;

namespace StarTrekTDD.Game.Ships
{
    public class KlingonShip : Ship
    {
        public Guid Id { get; }

        public KlingonShip(Guid id) : base(InitGameConfig.KlingonShipEnergyUnits)
        {
            Id = id;
        }

        public int Hit()
        {
            var random = new Random();
            var hitValue = random.Next(1, EnergyUnits);
            ReduceEnergyUnits(hitValue);
            return hitValue;
        }
    }
}