﻿using StarTrekTDD.Map.Structures;

namespace StarTrekTDD.Game.Ships
{
    public class Enterprise : Ship
    {
        public int StellarDates { get; private set; }
        public bool HasStellarDate => StellarDates > 0;

        public Enterprise() : base(InitGameConfig.EnergyUnits)
        {
            StellarDates = InitGameConfig.StellarDates;
        }

        public void ReduceResources(Distance distance)
        {
            StellarDates -= distance.MovedQuadrants;
            EnergyUnits -= distance.MovedSectors;
        }

        public void ResetEnergyUnits()
        {
            EnergyUnits = InitGameConfig.EnergyUnits;
        }

        public bool TryReduceEnergyUnits(int energyUnitsToUse)
        {
            if (energyUnitsToUse > EnergyUnits)
                return false;

            ReduceEnergyUnits(energyUnitsToUse);
            return true;
        }
    }
}