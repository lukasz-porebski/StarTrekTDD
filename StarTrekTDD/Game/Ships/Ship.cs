﻿namespace StarTrekTDD.Game.Ships
{
    public class Ship
    {
        public int EnergyUnits { get; protected set; }
        public bool HasEnergyUnits => EnergyUnits > 0;

        public Ship(int energyUnits)
        {
            EnergyUnits = energyUnits;
        }

        public void ReduceEnergyUnits(int energyUnitsToReduce) =>
            EnergyUnits -= energyUnitsToReduce;
    }
}