﻿using StarTrekTDD.Game.Enums;
using StarTrekTDD.Game.Ships;
using StarTrekTDD.Map;
using System;

namespace StarTrekTDD.Game
{
    public class Play
    {
        private readonly GameManager _gameManager = new GameManager(new Galaxy(), new Enterprise());
        private readonly UI _ui = new UI();

        public void Start()
        {
            CommandResult commandResult;

            do
            {
                Console.WriteLine();
                _ui.ShowEnterpriseStatus(_gameManager);
                var command = _ui.GetCorrectCommand();
                commandResult = RunCommand(command);
                DisplayCommandResult(commandResult);
            } while (!GameOver(commandResult));

            DisplayGameOverMessage(commandResult);
        }

        private CommandResult RunCommand(Command command)
        {
            switch (command)
            {
                case Command.MoveEnterprise:
                    var vector = _ui.GetVector();
                    return _gameManager.MoveEnterprise(vector);
                case Command.ShortScan:
                    _gameManager.DisplayEnterpriseQuadrant();
                    return CommandResult.None;
                case Command.LongScan:
                    _gameManager.DisplayQuadrantSurroundings();
                    return CommandResult.None;
                case Command.Battle:
                    var energyUnitsToUse = _ui.GetEnergyUnitsToUse();
                    return _gameManager.Fight(energyUnitsToUse);
                case Command.GalaxyMap:
                    _gameManager.DisplayGalaxyMap();
                    return CommandResult.None;
                default:
                    throw new ArgumentOutOfRangeException(nameof(command), command, null);
            }
        }

        private static void DisplayCommandResult(CommandResult commandResult)
        {
            if (commandResult.HasFlag(CommandResult.Moved))
                Console.WriteLine("Przemieszczono Enterprise");

            if (commandResult.HasFlag(CommandResult.KlingonShipsAttacked))
                Console.WriteLine("Statki Klingonów zaatakowały");

            if (commandResult.HasFlag(CommandResult.EnergyUnitHasRunOut))
                Console.WriteLine("Jednostki energii się skończyły");

            if (commandResult.HasFlag(CommandResult.StellarDateHasRunOut))
                Console.WriteLine("Daty gwiezdne się skończyły");

            if (commandResult.HasFlag(CommandResult.MovedToOccupiedSector))
                Console.WriteLine("Przemieszczono się na zajęty sektor");

            if (commandResult.HasFlag(CommandResult.EnergyUnitsRenewed))
                Console.WriteLine("Odnowiono jednostki energii");

            if (commandResult.HasFlag(CommandResult.SuccessfulHit))
                Console.WriteLine("Udany strzał");

            if (commandResult.HasFlag(CommandResult.FailHit))
                Console.WriteLine("Nieudany strzał");

            if (commandResult.HasFlag(CommandResult.SectorNotExists))
                Console.WriteLine("Sektor, do którego zmierzasz nie istnieje");
        }

        private static void DisplayGameOverMessage(CommandResult commandResult) =>
            Console.WriteLine(IsWin(commandResult) ? "Zwycięstwo!!! Koniec gry" : "Porażka. Koniec gry");

        private static bool GameOver(CommandResult commandResult) =>
            IsLost(commandResult) || IsWin(commandResult);

        private static bool IsWin(CommandResult commandResult) =>
            commandResult.HasFlag(CommandResult.DestroyedAllKlingonShips);

        private static bool IsLost(CommandResult commandResult) =>
            commandResult.HasFlag(CommandResult.EnergyUnitHasRunOut) ||
            commandResult.HasFlag(CommandResult.StellarDateHasRunOut) ||
            commandResult.HasFlag(CommandResult.MovedToOccupiedSector);
    }
}