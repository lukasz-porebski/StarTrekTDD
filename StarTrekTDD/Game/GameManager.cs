﻿using StarTrekTDD.Game.Enums;
using StarTrekTDD.Game.Ships;
using StarTrekTDD.Map;
using StarTrekTDD.Map.Enums;
using StarTrekTDD.Map.Structures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace StarTrekTDD.Game
{
    public class GameManager
    {
        public Galaxy Galaxy { get; }
        public Enterprise Enterprise { get; }
        public List<KlingonShip> KlingonShips { get; }
        public GalaxyDisplayer GalaxyDisplayer { get; }
        public List<QuadrantSurroundingsInfo> QuadrantSurroundingsInfosHistory { get; }

        public GameManager(Galaxy galaxy, Enterprise enterprise)
        {
            Galaxy = galaxy;
            Enterprise = enterprise;
            KlingonShips = Galaxy.KlingonShipSectors.Select(ks => new KlingonShip(ks.SpaceObject.Id)).ToList();
            GalaxyDisplayer = new GalaxyDisplayer(galaxy);
            QuadrantSurroundingsInfosHistory = new List<QuadrantSurroundingsInfo>();
        }

        public CommandResult MoveEnterprise(Vector2 distance)
        {
            var endPosition = Galaxy.EnterpriseSector.Vector + distance;
            if (!Galaxy.SectorExists(endPosition))
                return CommandResult.SectorNotExists;

            var distanceDetails = Galaxy.CalculateDistance(Galaxy.EnterpriseSector.Vector, distance);
            Enterprise.ReduceResources(distanceDetails);

            if (!Enterprise.HasStellarDate && Galaxy.IsAnyKlingonShipOutsideEnterpriseQuadrant())
                return CommandResult.StellarDateHasRunOut;

            if (distanceDetails.ChangedQuadrant)
                Galaxy.ShuffleQuadrant(distanceDetails.EndPosition.Quadrant);

            if (!Galaxy.IsSectorEmpty(distanceDetails.EndPosition.Sector))
                return CommandResult.MovedToOccupiedSector;

            var result = CommandResult.Moved;
            Galaxy.MoveEnterprise(distanceDetails.EndPosition.Sector);

            if (Galaxy.IsAnyKlingonShipInsideEnterpriseQuadrant())
                result |= StartKlingonShipsAttack();

            if (Galaxy.IsEnterpriseNextToStarFleetBase())
            {
                Enterprise.ResetEnergyUnits();
                result |= CommandResult.EnergyUnitsRenewed;
            }

            return result;
        }

        public CommandResult Fight(int energyUnitsToUse)
        {
            var result = CommandResult.None;

            if (energyUnitsToUse > 0)
            {
                if (!Enterprise.TryReduceEnergyUnits(energyUnitsToUse))
                    return CommandResult.EnergyUnitHasRunOut;

                var closetKlingonShipSector = Galaxy.GetEnterpriseSectorClosetKlingonShipSector();
                var klingonShip = KlingonShips.Single(ks => ks.Id == closetKlingonShipSector.SpaceObject.Id);

                var cityMetric = Distance.CalculateCityMetric(
                    Galaxy.EnterpriseSector.Vector, closetKlingonShipSector.Vector);
                var hitValue = GetHitValue(energyUnitsToUse, cityMetric);

                var hitResult = hitValue > 0 ? CommandResult.SuccessfulHit : CommandResult.FailHit;
                result |= hitResult;

                klingonShip.ReduceEnergyUnits(hitValue);

                if (!klingonShip.HasEnergyUnits)
                    RemoveKlingonShip(closetKlingonShipSector);
            }

            if (Galaxy.KlingonShipSectors.Any())
                result |= StartKlingonShipsAttack();
            else
                result |= CommandResult.DestroyedAllKlingonShips;

            return result;
        }

        public void DisplayEnterpriseQuadrant() =>
            GalaxyDisplayer.DisplayEnterpriseQuadrant();

        public void DisplayQuadrantSurroundings()
        {
            var info = Galaxy.GetEnterpriseQuadrantSurroundingsInfo();
            QuadrantSurroundingsInfosHistory.Add(info);
            GalaxyDisplayer.DisplayEnterpriseQuadrantSurroundings(info);
        }

        public void DisplayGalaxyMap()
        {
            foreach (var info in QuadrantSurroundingsInfosHistory)
                GalaxyDisplayer.DisplayEnterpriseQuadrantSurroundings(info);

            var newInfo = Galaxy.GetEnterpriseQuadrantSurroundingsInfo();
            QuadrantSurroundingsInfosHistory.Add(newInfo);
        }

        private CommandResult StartKlingonShipsAttack()
        {
            var result = CommandResult.KlingonShipsAttacked;
            var klingonShipSectors = Galaxy.GetKlingonShipSectorsInEnterpriseQuadrant();
            var klingonShips = KlingonShips.Where(ks => klingonShipSectors.Any(s => s.SpaceObject.Id == ks.Id));
            var hitSum = 0;

            foreach (var klingonShip in klingonShips)
            {
                hitSum += klingonShip.Hit();

                if (klingonShip.HasEnergyUnits)
                    continue;

                var klingonShipSector = klingonShipSectors.Single(ks => ks.SpaceObject.Id == klingonShip.Id);
                RemoveKlingonShip(klingonShipSector);
            }

            Enterprise.ReduceEnergyUnits(hitSum);

            if (!Enterprise.HasEnergyUnits)
                result |= CommandResult.EnergyUnitHasRunOut;

            return result;
        }

        private static int GetHitValue(int energyUnits, int cityMetric)
        {
            var random = new Random();
            var hitProbability = 5 / (cityMetric + 4);

            return hitProbability > random.NextDouble() ? energyUnits : 0;
        }

        private void RemoveKlingonShip(Sector klingonShipSector)
        {
            Galaxy.UpdateSectorSpaceObject(klingonShipSector, new SpaceObject(SpaceObjectType.Emptiness));
            KlingonShips.RemoveAll(ks => ks.Id == klingonShipSector.SpaceObject.Id);
        }
    }
}