﻿using StarTrekTDD.Game;

namespace StarTrekTDD
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var play = new Play();
            play.Start();
        }
    }
}
