﻿namespace StarTrekTDD
{
    public static class InitGameConfig
    {
        public const int KlingonShipsNumberInGalaxy = 7;
        public const int StarFleetBasesNumberInGalaxy = 2;
        public const int StarsNumberInGalaxy = 20;
        public const int EnterprisesNumberInGalaxy = 1;
        public const int StellarDates = 30;
        public const int EnergyUnits = 600;
        public const int QuadrantsNumberInGalaxy = GridSideSize * GridSideSize;
        public const int SectorsNumberInQuadrant = GridSideSize * GridSideSize;
        public const int GridSideSize = 8;
        public const int KlingonShipEnergyUnits = 100;
    }
}